## 1

2x2 identity matrix

$$
\begin{bmatrix}
1 & 0 \\
0 & 1
\end{bmatrix}
$$

## 2

Three vector definitions, horizontally

$$
\text{Votes}_{\text{User1}} =
\begin{bmatrix}
0 \\
0 \\
0 \\
0
\end{bmatrix}

\;\;\;\;\;

\text{Votes}_{\text{User2}} =
\begin{bmatrix}
0 \\
\frac{1}{2} \\
\frac{1}{2} \\
0
\end{bmatrix}

\;\;\;\;\;

\text{Votes}_{\text{User3}} =
\begin{bmatrix}
\frac{1}{3} \\
0 \\
\frac{1}{3} \\
\frac{1}{3} \\
\end{bmatrix}
$$

## 3

Two vector definitions, horizontally

$$
\text{Votes}_{\text{User1}} =
\begin{bmatrix}
0 \\
0 \\
0 \\
0
\end{bmatrix}

\;\;\;\;\;

\text{Votes}_{\text{User2}} =
\begin{bmatrix}
0 \\
\frac{1}{2} \\
\frac{1}{2} \\
0
\end{bmatrix}
$$

## 4

One vector definition

$$
\text{Votes}_{\text{User1}} =
\begin{bmatrix}
0 \\
0 \\
0 \\
0
\end{bmatrix}
$$

## 5

Same as 2, but with some newlines removed

$$
\text{Votes}_{\text{User1}} =
\begin{bmatrix}
0 \\
0 \\
0 \\
0
\end{bmatrix}
\;\;\;\;\;
\text{Votes}_{\text{User2}} =
\begin{bmatrix}
0 \\
\frac{1}{2} \\
\frac{1}{2} \\
0
\end{bmatrix}
\;\;\;\;\;
\text{Votes}_{\text{User3}} =
\begin{bmatrix}
\frac{1}{3} \\
0 \\
\frac{1}{3} \\
\frac{1}{3} \\
\end{bmatrix}
$$

## 6

Same as 3, but with some newlines removed

$$
\text{Votes}_{\text{User1}} =
\begin{bmatrix}
0 \\
0 \\
0 \\
0
\end{bmatrix}
\;\;\;\;\;
\text{Votes}_{\text{User2}} =
\begin{bmatrix}
0 \\
\frac{1}{2} \\
\frac{1}{2} \\
0
\end{bmatrix}
$$

## 7

An equation

$$
a^2+b^2=c^2
$$

## 8

A matrix multiplication equation

$$
\begin{bmatrix}
\frac{1}{4} & \frac{1}{4} & 0           & 0           & 0           & 0 & \frac{1}{3} & \frac{1}{3} \\
\frac{1}{4} & \frac{1}{4} & 0           & 0           & 0           & 0 & 0           & 0           \\
\frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 0 & 0           & 0           \\
\frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 0 & 0           & 0           \\
0           & 0           & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 0 & 0           & 0           \\
0           & 0           & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 1 & 0           & 0           \\
0           & 0           & 0           & 0           &           0 & 0 & \frac{1}{3} & \frac{1}{3} \\
0           & 0           & 0           & 0           &           0 & 0 & \frac{1}{3} & \frac{1}{3} \\
\end{bmatrix}
\begin{bmatrix}
1 \\
0 \\
0 \\
0 \\
0 \\
0 \\
0 \\
0 \\
\end{bmatrix}
=
\begin{bmatrix}
\frac{1}{4} \\
\frac{1}{4} \\
\frac{1}{4} \\
\frac{1}{4} \\
0           \\
0           \\
0           \\
0           \\
\end{bmatrix}
$$

## 9

Same as 9 with the `=` and RHS removed

$$
\begin{bmatrix}
\frac{1}{4} & \frac{1}{4} & 0           & 0           & 0           & 0 & \frac{1}{3} & \frac{1}{3} \\
\frac{1}{4} & \frac{1}{4} & 0           & 0           & 0           & 0 & 0           & 0           \\
\frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 0 & 0           & 0           \\
\frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 0 & 0           & 0           \\
0           & 0           & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 0 & 0           & 0           \\
0           & 0           & \frac{1}{4} & \frac{1}{4} & \frac{1}{4} & 1 & 0           & 0           \\
0           & 0           & 0           & 0           &           0 & 0 & \frac{1}{3} & \frac{1}{3} \\
0           & 0           & 0           & 0           &           0 & 0 & \frac{1}{3} & \frac{1}{3} \\
\end{bmatrix}
\begin{bmatrix}
1 \\
0 \\
0 \\
0 \\
0 \\
0 \\
0 \\
0 \\
\end{bmatrix}
$$